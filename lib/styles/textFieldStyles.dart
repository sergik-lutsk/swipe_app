import 'package:flutter/material.dart';

import 'appStyle.dart';

Widget appTextFieldWithoutImage(colorLeft, colorRight, String hint, bool hideText, TextCallback callback) {
  return Container(margin: const EdgeInsets.only(left: 61.0, right: 61.0),
    decoration: BoxDecoration(
      borderRadius: const BorderRadius.all(const Radius.circular(10.0)),
      gradient: LinearGradient(
          colors: [colorLeft, colorRight],
          begin: Alignment.centerLeft,
          end: Alignment.centerRight),
    ),
    height: 44.0,
    child: TextFormField(
      keyboardType: TextInputType.phone,
      cursorColor: primaryColor,
      obscureText: hideText,
      autofocus: false,
      style: new TextStyle(color: appWhiteTextColor),
      decoration: InputDecoration(
        border: InputBorder.none,
        contentPadding: EdgeInsets.fromLTRB(20.0, 0, 20.0, 0),
//        contentPadding: EdgeInsets.zero,
        hintText: hint,
        hintStyle: TextStyle(color: appTextHintColor),
      ),
      onChanged: (text) {
        callback(text);
      },
    ),
  );
}

Widget appTextFieldWithImage(FocusNode focusNode, TextEditingController textEditingController, String hint, String image, bool hideText, TextCallback callback) {
  return Container(
    height: 44.0,
    alignment: Alignment.center,
    child: TextField(
      cursorColor: primaryColor,
      obscureText: hideText,
      focusNode: focusNode,
      controller: textEditingController,
      autofocus: false,
      style: new TextStyle(color: appNormalTextColor),
      decoration: InputDecoration(
//          prefixIcon: Image.asset('assets/images/ic_drawer_account_green.png', width: 23.0, height: 23.0),
        prefixIcon:  Padding(
          padding: EdgeInsets.all(13.0),
          child: Image.asset('assets/images/' + image, width: 23.0, height: 23.0),
        ),
        contentPadding: EdgeInsets.fromLTRB(0.0, 14.0, 20.0, 6.0),
        hintText: hint,
        hintStyle: TextStyle(color: appTextHintColor),
        focusedBorder: const OutlineInputBorder(
          borderRadius: const BorderRadius.all(const Radius.circular(22.0)),
          borderSide: BorderSide(color: primaryColor, width: 2.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(const Radius.circular(22.0)),
          borderSide: BorderSide(color: appTextBorderColor, width: 2.0),
        ),
        fillColor: appTextFieldBackgroundColor,
        filled: true,
      ),
      onChanged: (text) {
        callback(text);
      },
    ),
  );
}

Widget appTextFieldUrl(String text, Color color) {
  return Container(margin: const EdgeInsets.only(left: 85.0, right: 85.0),
    child: Text(text,
      textAlign: TextAlign.center,
      style: new TextStyle(decoration: TextDecoration.underline,
      color: Colors.white,
      decorationColor: color,
      decorationStyle: TextDecorationStyle.solid,)
    ),
  );
}

Widget _myWidget(BuildContext context) {
  String myString = 'I ❤️ Flutter';
  print(myString);
  return Text(
    myString,
    style: TextStyle(fontSize: 30.0),
  );
}