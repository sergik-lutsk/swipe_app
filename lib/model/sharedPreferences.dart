import 'package:shared_preferences/shared_preferences.dart';

const String PREFERENCE_VALUE_IS_USER_EXIST = "PREFERENCE_VALUE_IS_USER_EXIST";
const String PREFERENCE_VALUE_USER_ID = "PREFERENCE_VALUE_USER_ID";
const String PREFERENCE_VALUE_USER_TOKEN = "PREFERENCE_VALUE_USER_TOKEN";
const String PREFERENCE_VALUE_USER_PHOTO = "PREFERENCE_VALUE_USER_PHOTO";
const String PREFERENCE_VALUE_USER_EMAIL = "PREFERENCE_VALUE_USER_EMAIL";
const String PREFERENCE_VALUE_USER_NAME = "PREFERENCE_VALUE_USER_NAME";

setStringPreferenceValue(String key, String value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString(key, value);
}

setIntPreferenceValue(String key, int value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setInt(key, value);
}

setBoolPreferenceValue(String key, bool value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setBool(key, value);
}

Future<String> getStringPreferenceValue(String key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString(key) ?? "";
}

Future<int> getIntPreferenceValue(String key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getInt(key) ?? 0;
}

Future<bool> getBoolPreferenceValue(String key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getBool(key) ?? false;
}

Future<SharedPreferences> prefs = SharedPreferences.getInstance();